package com.coin.database;

import com.coin.config.MainConfig;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DatabaseVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LogManager.getLogger(DatabaseVerticle.class);
    public static JDBCClient jdbcClient;

    @Override
    public void start() throws Exception {
        jdbcClient = JDBCClient.createShared(vertx, new JsonObject()
                .put("provider_class", "io.vertx.ext.jdbc.spi.impl.HikariCPDataSourceProvider")
                .put("jdbcUrl", MainConfig.getOracleDbConfig().getString("jdbcUrl"))
                .put("driverClassName", "com.mysql.cj.jdbc.Driver")
                .put("username", MainConfig.getOracleDbConfig().getString("username"))
                .put("password", MainConfig.getOracleDbConfig().getString("password"))
                .put("maximumPoolSize", MainConfig.getOracleDbConfig().getInteger("max_pool_size"))
        );
    }
}
