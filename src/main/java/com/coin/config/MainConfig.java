package com.coin.config;

import io.vertx.core.json.JsonObject;

public class MainConfig {
    private static JsonObject mainConfig;
    private static String emailOtpTemplate;

    private MainConfig() {};

    public static void setMainConfig(JsonObject mainConfig) {
        MainConfig.mainConfig = mainConfig;
    }

    public static JsonObject getOracleDbConfig() {
        return mainConfig.getJsonObject("mysql", new JsonObject());
    }
    public static JsonObject listPhoneConvert() {
        return new JsonObject();
    }
}
