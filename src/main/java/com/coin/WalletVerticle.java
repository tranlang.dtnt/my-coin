package com.coin;

import com.coin.database.MySqlManager;
import com.coin.util.DataUtil;
import com.coin.util.LogUtil;
import com.google.gson.Gson;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WalletVerticle extends AbstractVerticle {

    private static final Logger logger = LogManager.getLogger(WalletVerticle.class);
    private static final Gson PARSER = new Gson();
    private static final long TIME_EXPIRE = 180000; //3'

    @Override
    public void start() {
        HttpServer httpServer = vertx.createHttpServer();
        Router router = Router.router(vertx);

        router.post("/wallet").consumes("application/json").produces("application/json").handler(ctx -> {
            LogUtil.logHttpRequest(logger, ctx.request());
            final String logId = DataUtil.createRequestId();
            HttpServerResponse response = ctx.response();
            ctx.request().bodyHandler(buffer -> {

            });
        });

        httpServer.requestHandler(router).listen(1111);
    }
}
